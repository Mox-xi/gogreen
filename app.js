const express = require('express');
const bodyParser = require('body-parser');
const Sequelize = require('sequelize');
const cors = require('cors');
const app = express();
const db = require('./models/dbEngine');
const Account = require('./models/general/Account');
const Tasks = require('./models/general/Tasks');
const Tokens = require('./models/general/Tokens');
const Processin = require('./models/general/Processin');
const History = require('./models/general/History');
const Check = require('./models/general/Check');
const foptions = require('./actions/foptions');
const News = require('./models/general/News');

db.sync().then(result=>console.log("ok"))
    .catch(err=> console.log(err));


var corsOptions = {
    origin: true,
    methods: ["POST"],
    credentials: true,
};
app.use(express.json());
app.use(foptions());
app.use('/api/',cors(corsOptions),require('./routes/index')(express.Router()));
app.use('/api/admin',cors(corsOptions),require('./routes/admin/index')(express.Router()));


app.use(cors(corsOptions));


const PORT = 65533;
//const PORT = 5002;

app.listen(PORT,console.log(PORT));

app.use((req, res, next) => {
    res.statusCode = res.locals.status || 200;

    res.send({
        code: res.status,
        status: true,
        result: res.locals.data
    });
});
