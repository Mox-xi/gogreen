const {hist,accounts,inProc} = require('../../actions/admin/state');
const Admin = require('../../actions/admin/adminAuth');

module.exports = router => {

    router.post('/history',Admin(), (req, res, next) => {
        hist()
            .then(r => {
                res.locals.data = r;
                next('router');
            })
            .catch(next);
    });

    router.post('/accounts',Admin(), (req, res, next) => {
        accounts()
            .then(r => {
                res.locals.data = r;
                next('router');
            })
            .catch(next);
    });

    router.post('/proc',Admin(), (req, res, next) => {
        inProc()
            .then(r => {
                res.locals.data = r;
                next('router');
            })
            .catch(next);
    });


    return router;
};
