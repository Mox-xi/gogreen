const {createTask,deleteTask} = require('../../actions/admin/tasks');
const Admin = require('../../actions/admin/adminAuth');

module.exports = router => {

    router.post('/create',Admin(), (req, res, next) => {
        createTask(req.body)
            .then(r => {
                res.locals.data = r;
                next('router');
            })
            .catch(next);
    });

    router.post('/delete',Admin(), (req, res, next) => {
        deleteTask(req.body)
            .then(r => {
                res.locals.data = r;
                next('router');
            })
            .catch(next);
    });




    return router;
};
