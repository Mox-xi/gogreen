const express = require('express');

module.exports = router => {

    router.use('/task', require('./tasks')(express.Router()));
    router.use('/check', require('./check')(express.Router()));
    router.use('/news', require('./news')(express.Router()));
    router.use('/state', require('./state')(express.Router()));


    return router;
};



