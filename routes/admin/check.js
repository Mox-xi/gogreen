const {checkList,setMark} = require('../../actions/admin/tasks');
const Admin = require('../../actions/admin/adminAuth');

module.exports = router => {

    router.post('/list',Admin(), (req, res, next) => {
        checkList(req.body)
            .then(r => {
                res.locals.data = r;
                next('router');
            })
            .catch(next);
    });

    router.post('/mark',Admin(), (req, res, next) => {
        setMark(req.body)
            .then(r => {
                res.locals.data = r;
                next('router');
            })
            .catch(next);
    });


    return router;
};
