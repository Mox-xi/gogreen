const {createNews,listNews} = require('../../actions/admin/news');
const Admin = require('../../actions/admin/adminAuth');
const auth = require('../../actions/auth');

module.exports = router => {

    router.post('/create',Admin(), (req, res, next) => {
        createNews(req.body)
            .then(r => {
                res.locals.data = r;
                next('router');
            })
            .catch(next);
    });

    router.post('/list',auth(), (req, res, next) => {
        listNews()
            .then(r => {
                res.locals.data = r;
                next('router');
            })
            .catch(next);
    });

    return router;
};
