const {login,register,logout,profile,checkout,isAdmin,lvlup} = require("../actions/account");
const {accounts} = require('../actions/admin/state');
const Auth = require('../actions/auth');

module.exports = router => {

    router.post('/login',(req, res, next) => {
        login(req.body)
            .then(r => {
                res.locals.data = r;
                next('router');
            })
            .catch(next);
    });

    router.post('/reg', (req, res, next) => {
        register(req.body)
            .then(r => {
                res.locals.data = r;
                next('router');
            })
            .catch(next);
    });

    router.post('/logout',Auth(),(req, res, next) => {
        logout(req.headers,req.body)
            .then(r => {
                res.locals.data = r;
                next('router');
            })
            .catch(next);
    });

    router.post('/lvlup',Auth(),(req, res, next) => {
        lvlup(req.body)
            .then(r => {
                res.locals.data = r;
                next('router');
            })
            .catch(next);
    });

    router.post('/profile',Auth(),(req, res, next) => {
        profile(req.body)
            .then(r => {
                res.locals.data = r;
                next('router');
            })
            .catch(next);
    });

    router.post('/checkout',Auth(),(req, res, next) => {
        checkout(req.body)
            .then(r => {
                res.locals.data = r;
                next('router');
            })
            .catch(next);
    });

    router.post('/isadmin',Auth(),(req, res, next) => {
        isAdmin(req.body)
            .then(r => {
                res.locals.data = r;
                next('router');
            })
            .catch(next);
    });

    router.post('/friends',Auth(), (req, res, next) => {
        accounts()
            .then(r => {
                res.locals.data = r;
                next('router');
            })
            .catch(next);
    });


    return router;
};
