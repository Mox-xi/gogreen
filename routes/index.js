const express = require('express');

module.exports = router => {

    router.use('/', require('./account')(express.Router()));
    router.use('/quests', require('./quests')(express.Router()));


    return router;
};



