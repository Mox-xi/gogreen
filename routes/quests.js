const { listQuests,takeQuests,myQuests,turnQuests} = require("../actions/quests");
const Auth = require('../actions/auth');


module.exports = router => {

    router.post('/list',Auth(),(req, res, next) => {
        listQuests(req.body)
            .then(r => {
                res.locals.data = r;
                next('router');
            })
            .catch(next);
    });

    router.post('/take',Auth(),(req, res, next) => {
        takeQuests(req.body)
            .then(r => {
                res.locals.data = r;
                next('router');
            })
            .catch(next);
    });

    router.post('/myquests',Auth(),(req, res, next) => {
        myQuests(req.body)
            .then(r => {
                res.locals.data = r;
                next('router');
            })
            .catch(next);
    });

    router.post('/turn',Auth(),(req, res, next) => {
        turnQuests(req.body)
            .then(r => {
                res.locals.data = r;
                next('router');
            })
            .catch(next);
    });


    return router;
};
