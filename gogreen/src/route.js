import VueRouter from "vue-router";
import Vue from "vue"
import SingIn from "./components/SingIn";
import Reg from "./components/Registeration";
import Me from "./components/Me";
import Tasks from "./components/Tasks";
import fzf from './components/FzeroF';
import profile from './components/Profile';
import quests from './components/MyQuests';
import AdminPanel from "./components/Admin/AdminPanel";
import Check from "./components/Admin/Check";
import CheckPlace from "./components/Admin/CheckPlace";
import StartPage from "./components/StartPage";
import Initital from "./components/Initital";
import CheckPanel from "./components/CheckPanel";
import StateAccounts from "./components/Admin/StateAccounts";
import StateHistory from "./components/Admin/StateHistory";
import StateProc from "./components/Admin/StateProc";
import News from "./components/News";
import AboutHistory from "./components/Admin/AboutHistory";
import checkQuests from "./components/checkQuests";
import AboutNews from "./components/AboutNews";
import friends from "./components/friends";


Vue.use(VueRouter);


let router = new VueRouter({
    //mode: 'history',
    routes: [
        {
            path: '/friends',
            name: 'friends',
            component: friends
        },
        {
            path: '/',
            name: 'home',
            component: Initital
        },
        {
            path: '/singin',
            name: 'singIn',
            component: SingIn
        },
        {
            path: '/aboutnews',
            name: 'AboutNews',
            component: AboutNews
        },
        {
            path: '/reg',
            name: 'register',
            component: Reg
        },
        {
            path: '/tasks',
            name: 'secure',
            component: Tasks,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: '/me',
            name: 'me',
            component: Me,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: '/panel',
            name: 'Admin',
            component: AdminPanel,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: '/fzf',
            name: '404',
            component: fzf
        },
        {
            path: '/profile',
            name: 'profile',
            component: profile
        },
        {
            path: '/myquests',
            name: 'myquests',
            component: quests
        },
        {
            path: '/check',
            name: 'check',
            component: Check
        },
        {
            path: '/checkplace',
            name: 'checkPlace',
            component: CheckPlace
        },
        {
            path: '/start',
            name: 'StartPage',
            component: StartPage
        },
        {
            path: '/checkpanel',
            name: 'CheckPanel',
            component: CheckPanel
        },
        {
            path: '/accounts',
            name: 'StateAccounts',
            component: StateAccounts
        },
        {
            path: '/history',
            name: 'StateHistory',
            component: StateHistory
        },
        {
            path: '/proc',
            name: 'StateProc',
            component: StateProc
        },
        {
            path: '/news',
            name: 'news',
            component: News
        },
        {
            path: '/abouthistory',
            name: 'aboutHistory',
            component: AboutHistory
        },
        {
            path: '/quest',
            name: 'checkQuest',
            component: checkQuests
        },
    ]
});


export default router
