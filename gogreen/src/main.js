import Vue from 'vue';
import App from './App.vue';
import Axios from 'axios';
import router from "./route";
import VueKonva from 'vue-konva';

//import check from "./components/check";
import store from "./store";

import './registerServiceWorker'


const axiosInstance = Axios.create({
  baseURL: '/api',
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
  },
  timeout: 5000,
  withCredentials: true,
  maxContentLength: 20000,
});

Vue.prototype.$http = axiosInstance;

const token = localStorage.getItem('token');
if (token) {
  Vue.prototype.$http.defaults.headers['x-auth-token'] = token
}


Vue.use(VueKonva);


new Vue({
  router: router,
  store: store,
  render: h => h(App),
}).$mount('#app');
