/* eslint-disable no-console */

import { register } from 'register-service-worker'

const staticCacheName = 'static-cache-v0';
const staticAssets = [
    './',
    '../images/icons/icon-128x128.png',
    '../images/icons/icon-192x192.png',
];

if (process.env.NODE_ENV === 'production') {
register(`${process.env.BASE_URL}service-worker.js`, {
    ready () {
      console.log(
        'App is being served from cache by a service worker.\n' +
        'For more details, visit https://goo.gl/AFskqB'
      )
    },
    async registered () {
      console.log('Service worker has been registered.');
        const cache = await caches.open(staticCacheName);
        await cache.addAll(staticAssets);
        console.log('Content has been cached for offline use.')
    },
    async cached () {
        const cache = await caches.open(staticCacheName);
        await cache.addAll(staticAssets);
         console.log('Content has been cached for offline use.')
    },
    updatefound () {
      console.log('New content is downloading.')
    },
    updated () {
      console.log('New content is available; please refresh.')
    },
    offline (event) {
      console.log('No internet connection found. App is running in offline mode.')
        console.log('fetch');
        event.responseWith(caches.match(event.request).then(cachedResponse => {
            return cachedResponse || fetch(event.request);
        }))
    },
    fetch(){

    },
    error (error) {
      console.error('Error during service worker registration:', error)
    }
  });
}
