const {Model,DataTypes} = require("sequelize");
const Db = require('../dbEngine');

class Check extends Model {

    static findByName(name){
        return this.findOne({
            where:{
                name: name,
            }
        })
    }

    static findById(id){
        return this.findOne({
            where:{
                id: id,
            }
        })
    }

    static list(){
        return this.findAll({
            where:{}
        })
    }


}

Check.init ({
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false
    },
    description: {
        type: DataTypes.STRING,
        allowNull: false
    },
    worker: {
        type: DataTypes.STRING,
        allowNull: false
    },
    answer: {
        type: DataTypes.STRING,
        allowNull: false
    },
    points: {
        type: DataTypes.INTEGER,
        allowNull: false
    },

}, {sequelize: Db, modelName: 'Check' });

module.exports = Check;


