const {Model,DataTypes} = require("sequelize");
const Db = require('../dbEngine');

class Tokens extends Model {

    static findByToken(token){
        return this.findOne({
            where:{
                token: token,
            }
        })
    }

    static findByAccountId(id){
        return this.findOne({
            where:{
                accountId: id,
            }
        })
    }
}

Tokens.init ({
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false
    },
    accountId: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    token: {
        type: DataTypes.STRING,
        allowNull: false
    },
    date: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: Date.now(),
    },

}, {sequelize: Db, modelName: 'Tokens' });

module.exports = Tokens;


