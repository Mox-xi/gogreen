const {Model,DataTypes} = require("sequelize");
const Db = require('../dbEngine');

class Processin extends Model {


    static list(){
        return this.findAll({
            where:{}
        })
    }

    static findByAccAndTask(accountId,taskId){
        return this.findOne({
            where:{
                accountId: accountId,
                taskId: taskId
            }
        })
    }


    static findAllTasks(accountId){
        return this.findAll({
            where:{
                accountId: accountId,
            }
        })
    }

    static findTaskId(taskId){
        return this.findAll({
            where:{
                taskId: taskId,
            }
        })
    }


}

Processin.init ({
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false
    },
    accountId: {
        type: DataTypes.STRING,
        allowNull: false
    },
    taskId: {
        type: DataTypes.STRING,
        allowNull: false
    },

}, {sequelize: Db, modelName: 'Processin' });

module.exports = Processin;


