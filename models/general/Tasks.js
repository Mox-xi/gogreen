const {Model,DataTypes} = require("sequelize");
const Db = require('../dbEngine');

class Tasks extends Model {

    static findByName(name){
        return this.findOne({
            where:{
                name: name,
            }
        })
    }

    static findById(id){
        return this.findOne({
            where:{
                id: id,
            }
        })
    }

    static list(){
        return this.findAll({
            where:{}
        })
    }


}

Tasks.init ({
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false
    },
    description: {
        type: DataTypes.STRING,
        allowNull: false
    },
    countWorkers: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    points: {
        type: DataTypes.INTEGER,
        allowNull: false
    },

}, {sequelize: Db, modelName: 'Tasks' });

module.exports = Tasks;


