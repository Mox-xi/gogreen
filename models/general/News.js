const {Model,DataTypes} = require("sequelize");
const Db = require('../dbEngine');

class News extends Model {

    static findByName(name){
        return this.findOne({
            where:{
                name: name,
            }
        })
    }

    static findById(id){
        return this.findOne({
            where:{
                id: id,
            }
        })
    }

    static list(){
        return this.findAll({
            where:{}
        })
    }


}

News.init ({
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false
    },
    description: {
        type: DataTypes.STRING,
        allowNull: false
    },

}, {sequelize: Db, modelName: 'News' });

module.exports = News;


