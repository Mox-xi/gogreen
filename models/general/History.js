const {Model,DataTypes} = require("sequelize");
const Db = require('../dbEngine');

class History extends Model {

    static findByName(name){
        return this.findOne({
            where:{
                name: name,
            }
        })
    }

    static findById(id){
        return this.findOne({
            where:{
                id: id,
            }
        })
    }

    static list(){
        return this.findAll({
            where:{}
        })
    }


}

History.init ({
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false
    },
    description: {
        type: DataTypes.STRING,
        allowNull: false
    },
    worker: {
        type: DataTypes.STRING,
        allowNull: false
    },
    answer: {
        type: DataTypes.STRING,
        allowNull: false
    },
    points: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    checker: {
        type: DataTypes.STRING,
        allowNull: false
    },
    mark: {
        type: DataTypes.BOOLEAN,
        allowNull: false
    },

}, {sequelize: Db, modelName: 'History' });

module.exports = History;


