const {Model,DataTypes} = require("sequelize");
const Db = require('../dbEngine');

class Account extends Model {

    static findByLogin(login){
        return this.findOne({
            where:{
                login: login,
            }
        })
    }

    static findById(id){
        return this.findOne({
            where:{
                id: id,
            }
        })
    }

    static list(){
        return this.findAll({
            where:{}
        })
    }

}

Account.init ({
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false
    },
    login: {
        type: DataTypes.STRING,
        allowNull: false
    },
    email: {
        type: DataTypes.STRING,
        allowNull: false
    },
    password: {
        type: DataTypes.STRING,
        allowNull: false
    },
    isAdmin: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
    },
    lvlPoint: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0,
    },
    lvl: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 1,
    }
}, {sequelize: Db, modelName: 'Account' });

module.exports = Account;


