const Sequelize = require("sequelize");

let Db;
if (process.env.DATABASE) {
	Db = new Sequelize(process.env.DATABASE);
} else {
	Db = new Sequelize("game", "moxxi", "12", {
	    dialect: "postgres",
	    host: "localhost",
	    define: {
	        timestamps: false
	    }
	});
}

module.exports = Db;
