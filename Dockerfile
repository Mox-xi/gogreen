FROM node:14.4.0-stretch-slim

RUN apt-get -y update && apt-get install -y build-essential make python supervisor nginx gettext-base \
	&& rm -rf /var/lib/apt/lists/*

COPY ./nginx.conf /etc/nginx/sites-available/default.template

# Backend
WORKDIR /usr/src/backend
COPY ./ ./
RUN npm install

# Frontend
WORKDIR /usr/src/frontend
COPY ./gogreen ./
RUN npm install && npm run build \
	&& npm cache clean --force && rm -fr node_modules

COPY ./supervisord.conf /etc/supervisord.conf

ARG PORT

CMD /bin/bash -c "envsubst '\$PORT' < /etc/nginx/sites-available/default.template > /etc/nginx/sites-available/default"; /usr/bin/supervisord -c /etc/supervisord.conf
