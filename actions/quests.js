const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const Account = require('../models/general/Account');
const config = require('../tsconfig');
const Tasks = require('../models/general/Tasks');
const Processin = require('../models/general/Processin');
const Token = require('../models/general/Tokens');
const Check = require('../models/general/Check');

module.exports = {

    async  listQuests(headers) {
        try {
            const list = await Tasks.list();
            let res = [];
            for (const item of list) {
                if(item.countWorkers > 0){
                    res.push(item);
                }
            }
            return{
                status: "succsess",
                tasks: res
            }
        }
        catch(e)
        {
            return {
                status: false,
                error: e,
            }
        }
    },

    async  takeQuests(headers) {
        try {
            const task = await Tasks.findById(headers.taskid);
            const chk = await Processin.findByAccAndTask(headers.accountid.toString(),headers.taskid.toString());
            if(chk === null){
            task.countWorkers -=1;
            task.save();
            Processin.create({
                accountId: headers.accountid,
                taskId: headers.taskid,
            });
            return{
                status: "succsess",
            }}
            else{
                return{
                    status: "have",
                    mes: "you have already taken this quest"

                }
            }
        }
        catch(e)
        {
            return {
                status: false,
                error: e,
            }
        }
    },


    async  myQuests(headers) {
        try {
            const tasks = await Processin.findAllTasks(headers.accountid);
            let res = [];
            for (const item of tasks) {
                let one = await Tasks.findById(item.taskId);
                if (one !== null){
                res.push(one);
                }
            }
            return{
                    status: "succsess",
                    tasks: res
                }
        }
        catch(e)
        {
            return {
                status: false,
                error: e,
            }
        }
    },

    async  turnQuests(headers) {
        try {
            const task = await Tasks.findById(headers.taskid.toString());
            const user = await Account.findById(headers.accountid.toString());
            Check.create({
                    name: task.name,
                    description: task.description,
                    worker: user.login,
                    answer: headers.answer,
                    points: task.points,
            });
            const del = await Processin.findByAccAndTask(headers.accountid.toString(),headers.taskid.toString());
            del.destroy();
            const chk = await Processin.findTaskId(headers.taskid.toString());
            if(chk.length === 0){
                const dell = await Tasks.findById(headers.taskid.toString());
                if(dell.countWorkers === 0){
                    dell.destroy();
                }
            }
            return{
                    status: "succsess",
                }
        }
        catch(e)
        {
            return {
                status: false,
                error: e,
            }
        }
    },


};
