const Account = require('../../models/general/Account');
const Histories =require('../../models/general/History');
const Proc  = require('../../models/general/Processin');
const Tasks = require('../../models/general/Tasks');

module.exports = {

    async accounts(){
      try{
          const list = await Account.list();
          return {
              status: true,
              list: list,
          }
      }
      catch (e) {
          return {
              status: false,
          }
      }
    },

    async hist(){
        try{
            const list = await Histories.list();
            return {
                status: true,
                list: list,
            }
        }
        catch (e) {
            return {
                status: false,
            }
        }
    },

    async inProc(){
        try{
            const proc = await Proc.list();
            let res = [];
            for (const item of proc) {
                let one = await Tasks.findById(item.taskId);
                let name = await Account.findById(item.accountId);
                if (one !== null){
                    res.push({
                        worker: name.login,
                        task: one.name,
                        description: one.description,
                    });
                }
            }
            return {
                status: true,
                list: res,
            }
        }
        catch (e) {
            return {
                status: false,
            }
        }
    },



};