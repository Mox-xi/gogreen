const News = require('../../models/general/News');



module.exports = {

    async createNews(body){
        try {
            await News.create({
                name: body.name,
                description: body.description,
            });
            return {
                status: true
            }
        }catch(e){}
        {
         return {
             status: false,
             error: e
         }
        }
    },

    async listNews(){
        try {
            const list = await News.list();
            return {
                status: true,
                list: list.reverse()
            }
        }catch(e){}
        {
            return {
                status: false,
                error: e
            }
        }
    },

};