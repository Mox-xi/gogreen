const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const Account = require('../../models/general/Account');
const Task = require('../../models/general/Tasks');
const Check = require('../../models/general/Check');
const History = require('../../models/general/History');

module.exports = {
    async createTask(headers){
        try{
        await Task.create({
            name: headers.name,
            description: headers.description,
            points: headers.points,
            countWorkers: headers.cw,
        });
        return {
            status: "succsess"
        }
    }catch(e)
        {
            return {
                status: false,
                error: e,
            }
        }
    },


    async deleteTask(headers){
        try{
            const task = await Task.findByName(headers.name);
            task.destroy();
            return {
                status: "succsess"
            }
        }catch(e)
        {
            return {
                status: false,
                error: e,
            }
        }
    },

    async checkList(headers){
        try{
            const checkList = await Check.list();
            return {
                list: checkList,
                status: true
            }
        }catch(e)
        {
            return {
                status: false,
                error: e,
            }
        }
    },

    async setMark(headers){
        try{
            const chceker = await Account.findById(headers.accountid.toString());
            const papaer = await Check.findById(headers.checkid.toString());
            const worker = await Account.findByLogin(papaer.worker.toString());
            History.create({
                name: papaer.name,
                description: papaer.description,
                worker: papaer.worker,
                answer: papaer.answer,
                points: papaer.points,
                checker: chceker.login,
                mark: headers.mark
            });

            if(headers.mark === 'true'){
                worker.lvlPoint += papaer.points;
                await worker.save();
            }
            papaer.destroy();
            return {
                status: true
            }
        }catch(e)
        {
            return {
                status: false,
                error: e,
            }
        }
    }

};