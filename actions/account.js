const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const Account = require('../models/general/Account');
const config = require('../tsconfig');
const Token = require('../models/general/Tokens');


module.exports = {

    async login(body) {
        try {
        const resultAccount = await Account.findByLogin(body.login);
        if(!bcrypt.compareSync(body.password,resultAccount.password)){
            return {
                status: "wrong password"
            }
        }
        const payload = {
            id: resultAccount.id,
            login: resultAccount.login
        };
        const jwtToken = jwt.sign(payload, config.jwtSecret);

        const check = await Token.findByAccountId(resultAccount.id);

        if(check === null) {
            await Token.create({
                accountId: resultAccount.id,
                token: jwtToken,
                date: Date.now()
            });
        }
        else{
            check.token = jwtToken;
            check.date = Date.now();
            check.save();
        }
        return {
            login: resultAccount.login,
            accountId: resultAccount.id,
            lvl: resultAccount.lvl,
            status: true,
            token: jwtToken,
        }
        }catch(e)
        {
         return {
             status: false,
             error: e,
         }
        }
    },

    async register(body) {
        try {
            const resultAccount = await Account.findByLogin(body.login);
            if(resultAccount === null){
                const hash = await bcrypt.hash(body.password,10);
                Account.create({
                    login: body.login,
                    password: hash,
                    email: body.email,
                });
                return{
                    status: "succsess"
                }
            }
            else{
                return {
                    status: "account exist"
                }
            }
        }
        catch(e)
        {
            return {
                status: false,
                error: e,
            }
        }
    },


    async logout(headers,body) {
        const token = headers["x-auth-token"];
        try {
           const delet =  await Token.findByToken(token);
           delet.destroy();
            return {
               status: "logout"
            }
        }catch(e)
        {
            return {
                status: false,
                error: e,
            }
        }
    },

    async isAdmin(headers) {
        try {
            const user = await Account.findById(headers.accountid);
            if(user.isAdmin === true){
            return {
                status: "true"
            }
            }
            else {
                return {
                    status: "noAdmin"
                }
            }
        }catch(e)
        {
            return {
                status: false,
                error: e,
            }
        }
    },

    async profile(headers) {
        try {
            const user = await Account.findById(headers.accountid);
            if(user === null){
                return{
                    status: false
                }
            }

            return {
                status: true,
                login: user.login,
                points: user.lvlPoint,
                email: user.email,
                lvl: user.lvl,
                admin: user.isAdmin,
            }
        }catch(e)
        {
            return {
                status: false,
                error: e,
            }
        }
    },

    async checkout(headers) {
        const token = headers["x-auth-token"];
        try {
            const user =  await Token.findByToken(token);
            if(user === null){
            return {
                status: "false"
            }
            }
            else{
                return {
                    status: "true"
                }
            }
        }catch(e)
        {
            return {
                status: false,
                error: e,
            }
        }
    },

    async lvlup(headers) {
        try {
            const user = await Account.findById(headers.accountid);
            if(user === null){
                return{
                    status: false
                }
            }
            const points = user.lvl * 100;
            if(user.lvlPoint < points){
                return {
                    status: "have no to much points"
                }
            }

            user.lvlPoint -= points;
            user.lvl += 1;
            user.save();
            return {
                status: true
            }

        }catch(e)
        {
            return {
                status: false,
                error: e,
            }
        }
    },

};
