const Account = require('../models/general/Account');
const Token = require('../models/general/Tokens');
const config = require('../tsconfig');



const authMiddlewareFactory = (options = {}) => async(req, res, next) => {
    const tokenKey = req.headers['x-auth-token'];

    try {
        if (!tokenKey) {
            return next('Missing authorization token');
        }
        const result = await Token.findByToken(tokenKey);

        if(result === null){
            return next('noAuth');
        }

        const now = new Date();
        if (now - result.date >= config.jwtExpireIn) {
            return next('expireIn');
        }

        next();
    } catch (e) {
        next(e);
    }
};


module.exports = authMiddlewareFactory;