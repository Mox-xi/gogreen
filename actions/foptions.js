const foptions = (options = {}) => async(req, res, next) => {

    if(req.methods === 'OPTIONS'){
        res.send('');
        return
    }
    next();
};

module.exports = foptions;